FROM node:10-jessie as node
#If encounter Invalid cross-device error -run on host 'echo N | sudo tee /sys/module/overlay/parameters/metacopy'
ARG TORRENTS_CSV_ENDPOINT

COPY server/ui /app/server/ui
RUN cd /app/server/ui && yarn && yarn build

FROM rust:1.30 as rust
COPY server/service /app/server/service
RUN cd /app/server/service && cargo build --release

FROM debian:jessie-slim as volume

COPY torrents.csv  /db/

FROM debian:jessie-slim

RUN apt update && apt install -y sqlite3

COPY --from=node /app/server/ui/dist /app/dist
COPY --from=rust /app/server/service/target/release/torrents-csv-service /app/
COPY --from=volume /db/torrents.csv /app/
COPY scripts /app/scripts
RUN cd /app/scripts && . ./build_sqlite.sh

EXPOSE 8080
WORKDIR /app/
ARG TORRENTS_CSV_DB_FILE
RUN sqlite3 ${TORRENTS_CSV_DB_FILE} 'select * from torrents limit 10'

CMD /app/torrents-csv-service
