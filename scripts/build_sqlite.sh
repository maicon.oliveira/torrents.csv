#!/bin/bash
csv_file="${TORRENTS_CSV_FILE:-../torrents.csv}"
db_file="${TORRENTS_CSV_DB_FILE:-../torrents.db}"

echo "Creating temporary torrents.db file..."

# Remove double quotes for csv import
sed 's/\"//g' $csv_file > torrents_removed_quotes.csv

# Sort by seeders desc before insert
sort --field-separator=';' --key=5 -nr -o torrents_removed_quotes.csv torrents_removed_quotes.csv

rm $db_file

sqlite3 -batch $db_file <<"EOF"
create table torrents(
  "infohash" TEXT,
  "name" TEXT,
  "size_bytes" INTEGER,
  "created_unix" INTEGER,
  "seeders" INTEGER,
  "leechers" INTEGER,
  "completed" INTEGER,
  "scraped_date" INTEGER
);
.separator ";"
.import torrents_removed_quotes.csv torrents
UPDATE torrents SET completed=NULL WHERE completed = '';
EOF

rm torrents_removed_quotes.csv


