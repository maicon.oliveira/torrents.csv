#!/bin/bash

# Checking arguments
# Help line
cd ..
torrents_csv="`pwd`/torrents.csv"
scanned_out="`pwd`/infohashes_scanned.txt"

touch $scanned_out

help="Run ./scan_torrents.sh [TORRENTS_DIR] \nor goto https://gitlab.com/dessalines/torrents.csv for more help"
if [ "$1" == "-h" ] || [ -z "$1" ]; then
  echo -e $help
  exit 1
fi

torrents_dir="$1"
echo "Torrents dir=$torrents_dir"

# Check dependencies

if command -v "torrent-tracker-health" >/dev/null 2>&1 ; then
  echo "torrent-tracker-health installed."
else
  echo -e "Installing torrent-tracker-health:\nnpm i -g dessalines/torrent-tracker-health \nhttps://github.com/dessalines/torrent-tracker-health\n"
  npm i -g install dessalines/torrent-tracker-health
fi

# Loop over all torrents
pushd $torrents_dir
# for torrent_file in *.torrent; do
find `pwd` -name "*.torrent" | sort -n | grep -vFf $scanned_out | while read torrent_file ; do

  file_infohash=$(basename $torrent_file | cut -d'.' -f 1)

  # Scrape it
  health_text=$(torrent-tracker-health --torrent $torrent_file --timeout 1000 --addTrackers={udp://tracker.coppersurfer.tk:6969/announce,udp://tracker.internetwarriors.net:1337/announce,udp://tracker.opentrackr.org:1337/announce,udp://exodus.desync.com:6969/announce,udp://explodie.org:6969/announce})

  echo -e "$health_text"

  infohash=$(jq  -r '.hash' <<< $health_text)
  name=$(jq -r '.name' <<< $health_text)
  size_bytes=$(jq -r '.length' <<< $health_text)
  seeders=$(jq -r '.seeds' <<< $health_text)
  leechers=$(jq -r '.peers' <<< $health_text)
  completed=$(jq -r '.completed' <<< $health_text)
  date_string=$(jq -r '.created' <<< $health_text)
  if [ "$date_string"  == "null" ]; then
    echo "Date was null, setting to now"
    created_date=$(date +%s)
  else
    created_date=$(date -d "${date_string}" +"%s")
  fi
  scraped_date=$(date +%s)

  # Construct add line
  add_line="$infohash;$name;$size_bytes;$created_date;$seeders;$leechers;$completed;$scraped_date"
  # echo -e $add_line

  if (( $seeders > 0 )); then

    found_line=$(rg -n $infohash $torrents_csv)
    found_seeders=$(echo -e $found_line | cut -d';' -f 5)

    # Only re-add if the infohash doesn't exist, or 
    if [ ! -z "$found_line" ]; then

      # Seeder counts are different
      if [ "$found_seeders" != "$seeders" ]; then

        # Delete the original infohash line
        rg -Nv "$infohash" $torrents_csv > torfile.tmp.2; mv torfile.tmp.2 $torrents_csv

        # Append the add lines to the torrents.csv file
        echo -e "\n$add_line" >> $torrents_csv
        # truncate -s -1 $torrents_csv # Removing last newline
        echo -e "Updating Seeders:  $torrent_file | $name | $infohash | $seeders"
      else
        echo -e "Not adding $name, had identical seeders"
      fi
    else
      # Append the add lines to the torrents.csv file
      echo -e "\n$add_line" >> $torrents_csv
      # truncate -s -1 $torrents_csv # Removing last newline
      echo -e "New Torrent: $torrent_file | $name | $infohash | $seeders"
    fi

  else 
    # Deleting the line if it existed
    if [ ! -z "$infohash" ]; then
      # This removes lines that have no seeders
      echo -e "$name has no seeders"
      if rg -Nq $infohash $torrents_csv; then
        echo "Removing $name from $torrents_csv"
        rg -Nv "$infohash" $torrents_csv > torfile.tmp.2; mv torfile.tmp.2 $torrents_csv
      fi
    fi
  fi

  echo $file_infohash >> $scanned_out

done

popd
cd scripts
. prune.sh

