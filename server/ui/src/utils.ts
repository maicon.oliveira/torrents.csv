export function magnetLink(infohash: string, name: string): string {
  return `magnet:?xt=urn:btih:${infohash}&dn=${name}${trackerListToUrl(trackerList)}`;
}

let trackerList: Array<string> = [
  "udp://tracker.coppersurfer.tk:6969/announce",
  "udp://tracker.opentrackr.org:1337/announce",
  "udp://tracker.internetwarriors.net:1337/announce",
  "udp://9.rarbg.to:2710/announce",
  "udp://exodus.desync.com:6969/announce",
  "udp://tracker1.itzmx.com:8080/announce",
  "http://tracker3.itzmx.com:6961/announce",
  "udp://explodie.org:6969/announce"
];

function trackerListToUrl(trackerList: Array<string>): string {
  return trackerList.map(t => "&tr=" + t).join("");
}

export function humanFileSize(bytes, si): string {
  var thresh = si ? 1000 : 1024;
  if (Math.abs(bytes) < thresh) {
    return bytes + ' B';
  }
  var units = si
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
  var u = -1;
  do {
    bytes /= thresh;
    ++u;
  } while (Math.abs(bytes) >= thresh && u < units.length - 1);
  return bytes.toFixed(1) + ' ' + units[u];
}

export let repoUrl = 'https://gitlab.com/dessalines/torrents.csv';
