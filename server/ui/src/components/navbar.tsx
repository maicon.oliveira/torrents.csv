import { Component, linkEvent } from 'inferno';
import { SearchParams } from '../interfaces';
import { repoUrl } from '../utils';


interface State {
  searchParams: SearchParams;
}

export class Navbar extends Component<any, State> {

  state: State = {
    searchParams: {
      page: 1,
      q: ""
    }
  }

  constructor(props, context) {
    super(props, context);
    this.fillSearchField();
  }

  render() {
    return (
      <div class="sticky-top">{this.navbar()}</div>
    )
  }

  navbar() {
    return (
      <nav class="navbar navbar-dark navbar-purple p-1 shadow">
        <a class="navbar-brand mx-1" href="#">
          <i class="fas fa-fw fa-database mr-1"></i>
          Torrents.csv
        </a>
        <div class="navbar-nav ml-auto mr-2">
          <a class="nav-item nav-link" href={repoUrl}><i class="fab fa-fw fa-github"></i></a>
        </div>
        {this.searchForm()}
      </nav>
    );
  }

  searchForm() {
    return (
      <form class="col-12 col-sm-6 m-0 px-1" onSubmit={linkEvent(this, this.search)}>
        <div class="input-group w-100">
          <input class="form-control border-0 no-outline" type="search" placeholder="Search..." aria-label="Search..." required
                 value={this.state.searchParams.q}
                 onInput={linkEvent(this, this.searchChange)}></input>
          <button class="btn btn-light bg-white border-0 rounded-right no-outline" type="submit">
            <i className="fas fa-fw fa-search"></i>
          </button>
        </div>
      </form>
    );
  }

  search(i: Navbar, event) {
    event.preventDefault();
    i.context.router.history.push(`/search/${i.state.searchParams.q}/${i.state.searchParams.page}`);
  }

  searchChange(i: Navbar, event) {
    let searchParams: SearchParams = {
      q: event.target.value,
      page: 1
    }
    i.setState({ searchParams: searchParams });
  }

  fillSearchField() {
    let splitPath: Array<string> = this.context.router.route.location.pathname.split("/");
    if (splitPath.length == 4 && splitPath[1] == 'search')
      this.state.searchParams = {
        page: Number(splitPath[3]),
        q: splitPath[2]
      };
  }
}
